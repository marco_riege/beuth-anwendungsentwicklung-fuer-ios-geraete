//: Playground - noun: a place where people can play

import UIKit

enum Vehicle {
    case PKW(seriennummer: String)
    case LKW(seriennummer: String, achsen: Int, gewicht: Int)
}

func plausibilitaetspruefung(vehicleType: Vehicle) -> String {
    
    switch vehicleType {
    case .LKW:
        let gewichtproachse = (.LKW.gewicht / .LKW.achsen)
    case .PKW: break
        //PKW
    default:
        return "ungültig"
    }
    
    return "Test"
}

var pkw = Vehicle.PKW(seriennummer: "123456789BADA")
var lkw = Vehicle.LKW(seriennummer: "123456789BADA", achsen: 4, gewicht: 12)

print(plausibilitaetspruefung(vehicleType: pkw))
print(plausibilitaetspruefung(vehicleType: lkw))